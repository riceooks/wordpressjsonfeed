<?php
// WordPress
require(dirname(__FILE__) .'/wp-config.php');

// Json
require_once(ABSPATH .'/wp-includes/class-json.php');
$json = new Services_JSON();

// Params
$action = !empty($_REQUEST['action']) ? trim($_REQUEST['action']) : 'default';
$is_json = !empty($_REQUEST['json']) && intval($_REQUEST['json']) > 0 ? true : false;
$page = !empty($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
$limit = !empty($_REQUEST['limit']) && intval($_REQUEST['limit']) > 0 ? intval($_REQUEST['limit']) : 10;

switch( $action ){
	// 文章列表
	case 'posts':
		$args = array(
			'numberposts'     => $limit, // 筆數
			'offset'          => intval( ($page-1) * $limit ), // 略過筆數
			'orderby'         => 'post_date', // 排序
			'order'           => 'DESC', // 順序
			'post_type'       => 'post', // 類型
			'post_status'     => 'publish' // 狀態
			);
			
		// 取得文章
		$posts_array = get_posts( $args );
		
		if( $is_json ){
			// Json 格式輸出
			header('Content-type:application/json; charset=utf-8');
			echo $json->encode($posts_array);
		} else {
			// 陣列輸出
			header('Content-type:text/plain; charset=utf-8');
			print_r($posts_array);
		}
		break;
}

?>